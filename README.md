# Machine learning algorithms

This repos contains a collection of jupyter notebooks that implement the most common machine learning algorithms with python

Starting with a simple linear regression with an educational dataset to more advanced deep learning neural networks. We will cover how to use some real estate datasets that require hard pre-processing and analysis

## What you wil find here

This repos have a structure that follows the [Coursera machine learning course](https://fr.coursera.org/learn/machine-learning) from Andrew Ng.

It is meant to go deeper as my learning, skills and experience improve

#

### Algorithms
* [Simple linear regression](SimpleLinearRegression.ipynb)
  * Educational dataset
  * Scikitlearn and "By hand" with numpy gradient descent
* [Multiple linear regression](MultipleLinearRegression.ipynb)
  * Unknown dataset with clean data: standardization and label encoding
  * Scikitlearn and "By hand" with numpy gradient descent
* [Logistic Regression](LogisticRegression.ipynb)
  * Unknown educational dataset
  * Scikit-learn and "by hand" with scipy fmin minimizer
  * Boundary visualisation of polynomial hypothesis with regularization

#

### Contests

* [Datacrunch Hackathon](DatacrunchHackathon.ipynb)
  * Sweetviz data visualisation and PCA components selection
  * Data viualisation with TSNE and seaborn
  * polynomial features - one hot encoding
  * model selection
  * optuna parameter tuning

#

### Why this repo

I want to specialize myself into machine learning after a few years as a software engineer

Here is what I expect from this repo

* Learning
  * Implementing algorithms using the idiomatic python libraries
  * Using and testing existing frameworks with real estate datasets
* Personal resources index
  * Personal reminder for later implementations
  * Many links to useful external resources that explain more in depth the covered concepts
* Show off my skills
  * Give a fast overview of my machine learning skills and learning advancement
* Share and have returns
  * If you find some mistakes or approximations, please don't hesitate to create issues or message me
  * This is not intended to be an educational content, but this may be useful to someone

#

[clemaupetit@gmail.com](mailto:clemaupetit@gmail.com)