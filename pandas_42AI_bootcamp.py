
#%%

# Loading data

import pandas as pd
data = pd.read_csv('./datasets/athlete_events.csv')
data.shape

#%%
# Ex00 Load and display

def display_n(n):
    to_display = data.head(n) if n >= 0 else data.tail(-n)
    display(to_display)

display_n(-10)


#%%
# Ex01 min by age and year

def youngestFellah(data, y):
    return data.loc[data.Year == y, ['Age', 'Sex']].groupby('Sex').min()['Age'].to_dict()

youngestFellah(data, 2004)
for c in [2004, 0, -1, 1984]:
    display(youngestFellah(data, c))

data.groupby(['Year', 'Sex']).min(True)['Age']

#%%

# Ex02 get proportion by Gender Year and Sport

def proportionBySport(data, y, sport, gender):
    by_year = data.loc[data.Year == y].drop_duplicates('ID')
    total_gender = by_year['Sex'].value_counts()[gender]
    gender_by_sport = by_year.loc[by_year.Sport == sport]['Sex'].value_counts()[gender]
    return gender_by_sport / total_gender

proportionBySport(data, 2004, 'Tennis', 'F')

#%%
#%%

# Ex03 HowManyMedals

# d is an pandas Series (or SeriesGroupBy)
def output_format(d):
    d = d.to_dict()
    format_map = {'G': 'Gold', 'S': 'Silver', 'B': 'Bronze'}
    return {k: d.get(v, 0) for k, v in format_map.items()}

def howManyMedals(data, name):
    medal_counts = data.loc[data.Name == name].groupby(['Year', 'Medal'])['Medal'].count()
    years = medal_counts.index.get_level_values('Year').values
    return {year: output_format(medal_counts[year]) for year in years}

howManyMedals(data, 'Kjetil Andr Aamodt')

#%%
format_map = {'G': 'Gold', 'S': 'Silver', 'B': 'Bronze'}
for k, v in format_map.items():
    print(k, v)

#%%

# Ex04 SpatioTemporalData

def when(location):
    return data.loc[data.City == location]['Year'].unique().tolist()

def where(y):
    return data.loc[data.Year == y]['City'].unique().tolist()

display(when('Paris'))
display(where(2016))


#%%

# Ex05 HowManyMedalsByCountry

def howManyMedalsByCountry(country):
    by_country = data.loc[data.Team == country].groupby(['Year', 'Medal'])['Medal'].count()
    return {k: output_format(by_country[k]) for k, _ in by_country.groupby('Year')}

howManyMedalsByCountry('France')

#%%

# Ex06 - MyPlotLib
import seaborn as sns

def histogram(features):
    data[features].hist()

histogram(['Year', 'Medal', 'Weight'])
#%%

def density(features):
    data[features].plot.density()

density(['Year', 'Medal', 'Weight'])
#%%

def pair_plot(features):
    sns.pairplot(data=data[features])

pair_plot(['Year',  'Medal', 'Weight'])
#%%

import matplotlib.pyplot as plt
from pandas.api.types import is_numeric_dtype

def box(features):
    numeric_columns = [f for f in features if is_numeric_dtype(data[f])]
    for feature in numeric_columns:
        data[[feature]].boxplot()
        plt.show()

box(['Year', 'Medal', 'Weight'])
#%%

# Ex07 - Komparator

def compare_box_plots(cat_var, num_var):
    for cat in cat_var:
        data.boxplot(column=num_var, by=cat)

compare_box_plots(['Sex', 'City'], ['Height', 'Age', 'Weight'])

#%%

def compare_density(cat_var, num_var):
    for cat in cat_var:
        grouped = data.pivot(columns=cat, values=num_var)
        grouped.plot.kde()

compare_density(['Sex', 'City'], ['Height', 'Age'])

